<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'users-address');

Route::get('users-address', 'HomeController@index')->name('users-address');

Auth::routes();

Route::resource('address', 'Web\AddressController');

Route::resource('users', 'Web\UseController');
