@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="card">
					<h4 class="card-header bg-white">Editar Usuario
						<a href="{{ route('users.index') }}" class="btn btn-primary btn-sm float-right">volver</a>
					</h4>

					<div class="card-body">
						
						<form action="{{ route('users.update', $user->id) }}" method="POST">
							@csrf
							@method('PUT')

							<div class="form-group">
								<label for="full_name">Nombre Completo</label>
								<input type="text" class="form-control" name="full_name" id="full_name"  autofocus value="{{ $user->full_name }}">
							</div>

							<div class="form-group">
								<label for="short_name">Nombre Corto</label>
								<input type="text" class="form-control" name="short_name" id="short_name" value="{{ $user->short_name }}">
							</div>

							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" class="form-control" name="email" value="{{ $user->email }}">
							</div>

							<div class="form-group">
								<label for="phone">Telefono</label>
								<input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
							</div>

							<div class="form-group">
								<label for="reference">Donde nos conocio</label>
								<input type="text" class="form-control" name="reference" value="{{ $user->reference }}">
							</div>

							<br>

							<div class="form-group">
								<button type="submit" class="btn btn-sm btn-primary ">
									Guardar
								</button>
							</div>

						</form>
						
					</div>						
				</div>
			</div>
		</div>
	</div>



@endsection

