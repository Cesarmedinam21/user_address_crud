@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="card">
					<h4 class="card-header bg-white">Lista de Usuarios
						<a href="{{ route('address.index') }}" class="btn btn-primary btn-sm float-right">Volver a Direcciones</a>
					</h4>

					<div class="card-body">
						
						<table class="table">
							<thead>
								<tr>
									<th scope="col" width="10px">ID</th>
									<th scope="col">Nombre Corto</th>
									<th scope="col">Email</th>
									<th scope="col" colspan="3" class="text-center">Operaciones</th>
								</tr>
							</thead>
							@foreach ($users as $user)
								<tbody>
									<tr>
										<th scope="row">{{ $user->id }}</th>
										<td>{{ $user->short_name }}</td>
										<td>{{ $user->email }}</td>
										<td width="10px">
											<a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-primary">Ver</a>
										</td>
										<td width="10px">
											<a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-warning">Editar</a>
										</td>
										<td width="10px">

											<form action="{{ route('users.destroy', $user->id) }}" method="POST">
													@method('DELETE')
													@csrf
													<button type="submit" class="btn btn-sm btn-danger">
														Eliminar
													</button> 
											</form> 
											
										</td>
									</tr>
								</tbody>
							@endforeach
						</table>
						<div class="col-md-4 offset-md-4">
							{{ $users->render() }}
						</div>
						
					</div>						
				</div>
			</div>
		</div>
	</div>

@endsection
