@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="card">
					<h4 class="card-header bg-white">Visualizar Usuario
						<a href="{{ route('users.index') }}" class="btn btn-primary btn-sm float-right">volver</a>
					</h4>

					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th scope="col" width="10px">ID</th>
									<th scope="col">Nombre Completo</th>
									<th scope="col">Nombre Corto</th>
									<th scope="col">email</th>
									<th scope="col">Telefono</th>
									<th scope="col">Donde nos Conocio</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<th scope="row">{{ $user->id }}</th>
									<td>{{ $user->full_name }}</td>
									<td>{{ $user->short_name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->phone }}</td>
									<td>{{ $user->reference }}</td>
								</tr>
							</tbody>
						</table>
					</div>						
				</div>
			</div>
		</div>
	</div>

@endsection