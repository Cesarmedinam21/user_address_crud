@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="card">
					<h4 class="card-header bg-white">Editar Direccion
						<a href="{{ route('address.index') }}" class="btn btn-primary btn-sm float-right">volver</a>
					</h4>

					<div class="card-body">
						
						<form action="{{ route('address.update',  $address->id) }}" method="POST"> 
							
							@csrf
							@method('PUT')

							<input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

							<div class="form-group">
								<label for="country">Pais</label>
								<input type="text" class="form-control" name="country" value="{{ $address->country }}">
							</div>

							<div class="form-group">
								<label for="city">Ciudad</label>
								<input type="text" class="form-control" name="city" value="{{ $address->city }}">
							</div>

							<div class="form-group">
								<label for="street">Calle</label>
								<input type="text" class="form-control" name="street" value="{{ $address->street }}">
							</div>

							<div class="form-group">
								<label for="street_number">Numero de Calle</label>
								<input type="text" class="form-control" name="street_number" value="{{ $address->street_number }}">
							</div>

							<div class="form-group">
								<label for="postal_Code">Codigo postal</label>
								<input type="text" class="form-control" name="postal_Code" value="{{ $address->postal_Code }}">
							</div>

							<br>
							
							<div class="form-group">
								<button type="submit" class="btn btn-sm btn-primary ">
									Guardar
								</button>
							</div>

						</form>
						
					</div>						
				</div>
			</div>
		</div>
	</div>



@endsection

