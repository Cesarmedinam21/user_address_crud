@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="card">
                    <h4 class="card-header bg-white">Lista de Direcciones
                        <a href="{{ route('address.create') }}" class="btn btn-primary btn-sm float-right">Crear</a>
                    </h4>

                    <div class="card-body">
                        
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" width="10px">ID</th>
                                    <th scope="col">Pertenece a</th>
                                    <th scope="col">Pais</th>
                                    <th scope="col">Ciudad</th>
                                    <th scope="col">Codigo postal</th>
                                    <th scope="col" colspan="3" class="text-center">Operaciones</th>
                                </tr>
                            </thead>
                            @foreach ($addresses as $address)
                                <tbody>
                                    <tr>
                                        <th scope="row">{{ $address->id }}</th>
                                        <td>{{ $address->user->short_name }}</td>
                                        <td>{{ $address->country }}</td>
                                        <td>{{ $address->city }}</td>
                                        <td>{{ $address->postal_Code }}</td>
                                        
                                        <td width="10px">
                                            <a href="{{ route('address.show', $address->id) }}" class="btn btn-sm btn-primary">Ver</a>
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('address.edit', $address->id) }}" class="btn btn-sm btn-warning">Editar</a>
                                        </td>
                                        <td width="10px">

                                            <form action="{{ route('address.destroy', $address->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-danger">
                                                        Eliminar
                                                    </button> 
                                            </form> 
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                        <div class="col-md-4 offset-md-4">
                            {{ $addresses->render() }}
                        </div>
                        
                    </div>                      
                </div>
            </div>
        </div>
    </div>

@endsection
