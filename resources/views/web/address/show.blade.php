@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="card">
					<h4 class="card-header bg-white">Visualizar Direccion
						<a href="{{ route('address.index') }}" class="btn btn-primary btn-sm float-right">volver</a>
					</h4>

					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th scope="col" width="10px">ID</th>
									<th scope="col">Pertenece a</th>
									<th scope="col">Pais</th>
									<th scope="col">Ciudad</th>
									<th scope="col">Calle</th>
									<th scope="col">Numero de Calle</th>
									<th scope="col">Codigo Postal</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<th scope="row">{{ $address->id }}</th>
									<td>{{ $address->user->short_name }}</td>
									<td>{{ $address->country }}</td>
									<td>{{ $address->city }}</td>
									<td>{{ $address->street }}</td>
									<td>{{ $address->street_number }}</td>
									<td>{{ $address->postal_Code }}</td>
								</tr>
							</tbody>
						</table>
					</div>						
				</div>
			</div>
		</div>
	</div>

@endsection