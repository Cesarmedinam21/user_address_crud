<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
//Validation Request
use App\Http\Requests\AddressStoreRequest;
use App\Http\Requests\AddressUpdateRequest;


use App\Http\Controllers\Controller;
use App\Shipping_Address;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Shipping_Address::orderBy('id', 'DESC')->paginate();
        return view('web.address.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('web.address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressStoreRequest $request)
    {
        $address = Shipping_Address::create($request->all());

        return redirect()->route('address.edit', $address->id)->with('info', 'Direccion creada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $address = Shipping_Address::find($id);
        return view('web.address.show', compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = Shipping_Address::find($id);
        return view('web.address.edit', compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddressUpdateRequest $request, $id)
    {
        $address = Shipping_Address::find($id);
        $address->fill($request->all())->save();

        return redirect()->route('address.edit', $address->id)->with('info', 'Direccion actualizada con exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Shipping_Address::find($id);
        $address->delete();
        return back()->with('info', 'Direccion eliminada con exito');
    }
}
