<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipping_Address;

class HomeController extends Controller
{
    
    public function index()
    {
        $addresses = Shipping_Address::orderBy('id', 'DESC')->paginate();
        return view('address', compact('addresses'));
    }
}
