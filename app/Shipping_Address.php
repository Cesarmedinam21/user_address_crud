<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_Address extends Model
{
    protected $fillable = [
        'user_id', 'country', 'city', 'street','street_number', 'postal_Code', 
    ];

    public function user()//una direccion de envio le pertenece a un usuario
	{
		return $this->belongsTo(User::class);
	}
}
